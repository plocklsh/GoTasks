package logger

import (
	"fmt"
	"log"
	"os"
	"runtime"
	"strconv"
	"sync"
	"time"
    "bytes"
)

const (
	_VER string = "1.0.0"
)

//type LEVEL int32

type UNIT int64

const (
	_       = iota
	KB UNIT = 1 << (iota * 10)
	MB
	GB
	TB
)

const (
	LOG int = iota
	DEBUG
	WARN
	ERROR
	FATAL
)

const (
    OS_LINUX = iota
    OS_X
    OS_WIN
    OS_OTHERS
)

type _FILE struct {
	dir      string
	filename string
	_suffix  int
	isCover  bool
	_date    *time.Time
	mu       *sync.RWMutex
	logfile  *os.File
	lg       *log.Logger
}

var logLevel int = 1
var maxFileSize int64
var maxFileCount int32
var dailyRolling bool = true
var consoleAppender bool = true
var RollingFile bool = false
var logObj *_FILE

//var timeFormat string = "2006-01-02 15:04:05"
var consoleFormat string = "%s:%d %s %s"
var logFormat string = "%s %s"

const DATEFORMAT = "2006-01-02"

func SetConsole(isConsole bool) {
	consoleAppender = isConsole
}

func SetLevel(_level int) {
	logLevel = _level
}

func SetRollingFile(fileDir, fileName string, maxNumber int32, maxSize int64, _unit UNIT) {
	maxFileCount = maxNumber
	maxFileSize = maxSize * int64(_unit)
	RollingFile = true
	dailyRolling = false
	logObj = &_FILE{dir: fileDir, filename: fileName, isCover: false, mu: new(sync.RWMutex)}
	logObj.mu.Lock()
	defer logObj.mu.Unlock()
	for i := 1; i <= int(maxNumber); i++ {
		if isExist(fileDir + "/" + fileName + "." + strconv.Itoa(i)) {
			logObj._suffix = i
		} else {
			break
		}
	}
	if !logObj.isMustRename() {
		logObj.logfile, _ = os.OpenFile(fileDir+"/"+fileName, os.O_RDWR|os.O_APPEND|os.O_CREATE, 0)
		logObj.lg = log.New(logObj.logfile, "\n", log.Ldate|log.Ltime|log.Lshortfile)
	} else {
		logObj.rename()
	}
	go fileMonitor()
}

func SetRollingDaily(fileDir, fileName string) {
	RollingFile = false
	dailyRolling = true
	t, _ := time.Parse(DATEFORMAT, time.Now().Format(DATEFORMAT))
	logObj = &_FILE{dir: fileDir, filename: fileName, _date: &t, isCover: false, mu: new(sync.RWMutex)}
	logObj.mu.Lock()
	defer logObj.mu.Unlock()

	if !logObj.isMustRename() {
		logObj.logfile, _ = os.OpenFile(fileDir+"/"+fileName, os.O_RDWR|os.O_APPEND|os.O_CREATE, 0)
		logObj.lg = log.New(logObj.logfile, "\n", log.Ldate|log.Ltime|log.Lshortfile)
	} else {
		logObj.rename()
	}
}

func GetTraceLevelName(level int) string {
    switch level {
        case LOG :
        return "LOG"
        case DEBUG :
        return "DEBUG"
        case WARN :
        return "WARN"
        case ERROR :
        return "ERROR"
        case FATAL :
        return "FATAL"
        default :
        return "UNKNOWN"
    }
}

func GetOsFlag() int {
    switch os := runtime.GOOS; os {
        case "darwin":
        return OS_X
        case "linux":
        return OS_LINUX
        case "windows":
        return OS_WIN
        default:
        return OS_OTHERS
    }
}

func GetOsEol() string {
    if GetOsFlag() == OS_WIN {
        return "\r\n"
    }
    return "\n"
}

func Concat(delimiter string, input ...interface{}) string {
    buffer := bytes.Buffer{}
    l := len(input)
    for i := 0; i < l; i++ {
        str := fmt.Sprint(input[i])
        buffer.WriteString(str)
        if i < l - 1 {
            buffer.WriteString(delimiter)
        }
    }
    return buffer.String()
}

func console(msg string) {
	if consoleAppender {
		log.Print(msg)
	}
}

func getTraceFileLine() (string, int) {
    _, file, line, _ := runtime.Caller(2)
    short := file
    for i := len(file) - 1; i > 0; i-- {
        if file[i] == '/' {
            short = file[i+1:]
            break
        }
    }
    file = short
    return file, line
}

func buildConsoleMessage(level int, msg string) string {
    file, line := getTraceFileLine()
    return fmt.Sprintf(consoleFormat + GetOsEol(),  file, line, GetTraceLevelName(level), msg)
}

func buildLogMessage(level int, msg string) string {
    return fmt.Sprintf(logFormat + GetOsEol(), GetTraceLevelName(level), msg)
}

func catchError() {
	if err := recover(); err != nil {
		log.Println("err", err)
	}
}

func Trace(level int, v ...interface{}) bool {
    if dailyRolling {
        fileCheck()
    }
    defer catchError()
    logObj.mu.RLock()
    defer logObj.mu.RUnlock()

    msg := Concat(" ", v...)

    console(buildConsoleMessage(level, msg))

    if level >= logLevel {
        logObj.lg.Output(2, buildLogMessage(level, msg))
    }

    return true
}

func Log(v ...interface{}) bool {
    return Trace(LOG, v...)
}

func Debug(v ...interface{}) bool {
    return Trace(DEBUG, v...)
//	if dailyRolling {
//		fileCheck()
//	}
//	defer catchError()
//	logObj.mu.RLock()
//	defer logObj.mu.RUnlock()
//
//	if logLevel <= DEBUG {
//		logObj.lg.Output(2, fmt.Sprintln("debug", v))
//		console("debug", v)
//	}
}
//func Info(v ...interface{}) {
//	if dailyRolling {
//		fileCheck()
//	}
//	defer catchError()
//	logObj.mu.RLock()
//	defer logObj.mu.RUnlock()
//	if logLevel <= INFO {
//		logObj.lg.Output(2, fmt.Sprintln("info", v))
//		console("info", v)
//	}
//}

func Warn(v ...interface{}) bool {
    return Trace(DEBUG, v...)
//	if dailyRolling {
//		fileCheck()
//	}
//	defer catchError()
//	logObj.mu.RLock()
//	defer logObj.mu.RUnlock()
//	if logLevel <= WARN {
//		logObj.lg.Output(2, fmt.Sprintln("warn", v))
//		console("warn", v)
//	}
}

func Error(v ...interface{}) bool {
    return Trace(ERROR, v...)
//	if dailyRolling {
//		fileCheck()
//	}
//	defer catchError()
//	logObj.mu.RLock()
//	defer logObj.mu.RUnlock()
//	if logLevel <= ERROR {
//		logObj.lg.Output(2, fmt.Sprintln("error", v))
//		console("error", v)
//	}
}

func Fatal(v ...interface{}) bool {
    return Trace(FATAL, v...)
//	if dailyRolling {
//		fileCheck()
//	}
//	defer catchError()
//	logObj.mu.RLock()
//	defer logObj.mu.RUnlock()
//	if logLevel <= FATAL {
//		logObj.lg.Output(2, fmt.Sprintln("fatal", v))
//		console("fatal", v)
//	}
}

func (f *_FILE) isMustRename() bool {
	if dailyRolling {
		t, _ := time.Parse(DATEFORMAT, time.Now().Format(DATEFORMAT))
		if t.After(*f._date) {
			return true
		}
	} else {
		if maxFileCount > 1 {
			if fileSize(f.dir+"/"+f.filename) >= maxFileSize {
				return true
			}
		}
	}
	return false
}

func (f *_FILE) rename() {
	if dailyRolling {
		fn := f.dir + "/" + f.filename + "." + f._date.Format(DATEFORMAT)
		if !isExist(fn) && f.isMustRename() {
			if f.logfile != nil {
				f.logfile.Close()
			}
			err := os.Rename(f.dir+"/"+f.filename, fn)
			if err != nil {
				f.lg.Println("rename err", err.Error())
			}
			t, _ := time.Parse(DATEFORMAT, time.Now().Format(DATEFORMAT))
			f._date = &t
			f.logfile, _ = os.Create(f.dir + "/" + f.filename)
			f.lg = log.New(logObj.logfile, "\n", log.Ldate|log.Ltime|log.Lshortfile)
		}
	} else {
		f.coverNextOne()
	}
}

func (f *_FILE) nextSuffix() int {
	return int(f._suffix%int(maxFileCount) + 1)
}

func (f *_FILE) coverNextOne() {
	f._suffix = f.nextSuffix()
	if f.logfile != nil {
		f.logfile.Close()
	}
	if isExist(f.dir + "/" + f.filename + "." + strconv.Itoa(int(f._suffix))) {
		os.Remove(f.dir + "/" + f.filename + "." + strconv.Itoa(int(f._suffix)))
	}
	os.Rename(f.dir+"/"+f.filename, f.dir+"/"+f.filename+"."+strconv.Itoa(int(f._suffix)))
	f.logfile, _ = os.Create(f.dir + "/" + f.filename)
	f.lg = log.New(logObj.logfile, "\n", log.Ldate|log.Ltime|log.Lshortfile)
}

func fileSize(file string) int64 {
//	fmt.Println("fileSize", file)
	f, e := os.Stat(file)
	if e != nil {
		fmt.Println(e.Error())
		return 0
	}
	return f.Size()
}

func isExist(path string) bool {
	_, err := os.Stat(path)
	return err == nil || os.IsExist(err)
}

func fileMonitor() {
	timer := time.NewTicker(1 * time.Second)
	for {
		select {
		case <-timer.C:
			fileCheck()
		}
	}
}

func fileCheck() {
	defer func() {
		if err := recover(); err != nil {
			log.Println(err)
		}
	}()
	if logObj != nil && logObj.isMustRename() {
		logObj.mu.Lock()
		defer logObj.mu.Unlock()
		logObj.rename()
	}
}
