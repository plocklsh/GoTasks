#GoTasks

呃，不用说明了吧，直接go build就可以随意玩了。

已通过编译的版本包括：`go1.2.1 linux/amd64`、`go1.4.2 windows/amd64`。

目前我已经持续运行了超过12个小时，15个任务，其中有2个是秒发的任务，非常稳定没有中断过。

Http任务的执行是并发的，绝不延迟，同一个任务，下一次执行不会受到上一次任务的延迟而迟滞，所以请确定接收服务器能承受住压力才好使用。

日志库用了这个[https://github.com/donnie4w/go-logger](https://github.com/donnie4w/go-logger)，不过我修改了一些。

##启动参数

```
./GoTasks --help
Usage of ./GoTasks:
  -cycle=43200: 重新加载任务的周期，单位秒
  -log-dir=".": 日志输出目录
  -log-level=1: 日志输出级别
  -show-complete=0: 显示请求完成的信息
  -tasks="": 任务列表
  -trace-cycle=600: 输出心跳信息的周期，单位秒
  -test=false: 是否测试命令的参数
  -console=true: 是否在命令行输出调试
```

执行命令

```
GoTasks -tasks="http://localhost/tasks.json" -cycle=43200
```

##tasks.json输出格式

给一个tasks.json的清单样例：

```json
{
	"Tasks":[
		{
			"Name":"kissOschina",
			"Url":"http://www.oschina.net/",
			"Cycle":1,
			"PostUrl":""
		},
		{
			"Name":"kissBaidu",
			"Url":"http://www.baidu.com/",
			"Cycle":1,
			"PostUrl":""
		}
	]
}
```

* Name 任务名称
* Url 要取得数据的Url
* Cycle 获取的周期，单位秒
* PostUrl 从Url取得的页面的内容，将他Post到另一个Url上

##linux服务器发布

修改go_tasks.sh文件，修改13-35行部分的内容，主要填写：

* DAEMON GoTasks执行文件所在
* TASKS 任务URL

其它看需要修改。执行参数：

```bash
go_tasks.sh start    # 启动
go_tasks.sh stop     # 停止
go_tasks.sh restart  # 重启
go_tasks.sh status   # 查看状态
go_tasks.sh proc     # 查看具体的进程
```

目前限定单进程执行，如果要跑多个进程，最好将复制多一套执行文件和启动脚本，然后就可以多个进程了。